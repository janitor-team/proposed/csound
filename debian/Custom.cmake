

set(Python_ADDITIONAL_VERSIONS "@PYVERSION@")
#set(JAVA_MODULE_INSTALL_DIR "")
set(LUA_MODULE_INSTALL_DIR "lib/lua/5.1/")
set(EIGEN3_INCLUDE_DIR "/usr/include/")
find_library(LUAJIT_LIBRARY lua5.1)
set(LUAJIT_INCLUDE_DIR "/usr/include/lua5.1")
find_path(LUA_H_PATH lua.h ${LUAJIT_INCLUDE_DIR})
include_directories(${LUAJIT_INCLUDE_DIR})

# Enable optimizations
add_compile_options(-ftree-vectorize -ffast-math -O3 -fno-finite-math-only)
# Only in 386 or amd64 enable mtune, specified from the rules file
set(mtuneopt "@MTUNE@")
set(simdopt "@ENABLE_SIMD@")
if(mtuneopt)
    add_compile_options("-mtune=generic")
endif()
if(NOT simdopt)
    add_definitions("-DPFFFT_SIMD_DISABLE")
endif()

add_definitions(-DCS_PACKAGE_DATE=\"@DATE@\")

#set(FLTK_INCLUDE_DIR "/usr/include")
set(FLTK_SKIP_OPENGL ON)

